import { mount } from '@vue/test-utils'
import OutLink from '@/components/OutLink.vue'

describe('OutLink', () => {
  let component

  const create = ({ href, title }) => {
    component = mount(OutLink, { propsData: { href, title } })
  }

  it('renders the link', () => {
    create({ href: 'http://example.com' })
    expect(component.html()).toBe(
      '<a href="http://example.com" target="_blank" rel="noopener noreferrer" title=""></a>'
    )
  })

  describe('with title', () => {
    create({ href: 'http://example.com', title: 'Example Link' })
    expect(component.attributes('title')).toBe('Example Link')
  })
})
